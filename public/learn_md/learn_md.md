####`https://shd101wyy.github.io/markdown-preview-enhanced/#/`
## MD to PDF
make pdf: `pandoc learn.md -s -o test1.pdf`

## MD to HTML
make pdf: `pandoc learn.md -s -o test1.html`

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

regular text

**bold text**

*itlaic text* 

line(space space)  
break

<mark>Highlight on HTML</mark>

> Blockquote

>> Nested blockquote

[Link Text](http://google.com)

<http://google.com>

--- 

* Bulleted List Item
1. Numbered List Item

Inline `code`

```code block```

| First Header  | Second Header |
| ------------- | ------------- |
| Content Cell  | Content Cell  |
| Content Cell  | Content Cell  |

&uparrow;
&downarrow;
&rightarrow;
&leftarrow;

<sup>1</sup>&frasl;<sub>2</sub>
 $\color{red}{colors}$
 
 # HTML only :(
==marked==  

---
Image online: 
![alt text](http://www.dogbreedplus.com/dog_breeds/images/italiangreyhound1.jpg "Optional Title Text")
My own Image resized: <img src="https://gitlab.com/CourtneyFarnsworth/notes/raw/master/public/imgs/learn_md/dog.jpg" height=200>

